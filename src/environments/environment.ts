export const environment = {
  host: 'https://kolab-plumboomteam-api.herokuapp.com',
  production: false,
  tokenKey: 'TOKEN',
};
